class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;

    if (grades.length === 4) {
      if (grades.every(grade => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }

  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }

  computeAve() {
    let sum = 0;
    this.grades.forEach(grade => (sum = sum + grade));
    this.gradeAve = sum / 4;
    return this;
  }

  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }

  willPassWithHonors() {
    if (this.passed) {
      if (this.gradeAve >= 90) {
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
} // ==== end of class Student ====

class Section {
  constructor(name) {
    this.name = name;
    this.students = [];
    this.honorStudents = undefined;
    this.honorsPercentage = undefined;
  }

  addStudent(name, email, grades) {
    this.students.push(new Student(name, email, grades));
    return this;
  }

  countHonorStudents() {
    let count = 0;
    this.students.forEach(student => {
      if (student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
        count++;
      }
    });
    this.honorStudents = count;
    return this;
  }

  computeHonorsPercentage() {
    this.countHonorStudents();
    this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
    return this;
  }
} // ==== end of class Section ====

class Grade {
  // 1.
  constructor(level) {
    this.level = level;
    this.sections = [];
    this.totalStudents = 0;
    this.totalHonorStudent = 0;
    this.batchAveGrade = undefined;
    this.batchMinGrade = undefined;
    this.batchMaxGrade = undefined;
  }
  //2.
  addSection(section) {
    this.sections.push(new Section(section));
    return this;
  }
  //4.
  countStudents() {
    let numStudent = 0;
    this.sections.forEach(elem => (numStudent += elem.students.length));
    this.totalStudents = numStudent;
    return this;
  }
  //5.
  countHonorStudents() {
    let numOfHonors = 0;
    this.sections.forEach(elem => {
      elem.countHonorStudents();
      numOfHonors += elem.honorStudents;
    });
    this.totalHonorStudent = numOfHonors;
    return this;
  }
  //6.
  computeBatchAve() {
    let getAllAveGrade = 0;
    if (this.totalStudents) {
      this.sections.forEach(elem => {
        elem.students.forEach(grade => {
          getAllAveGrade += grade.gradeAve;
        });
      });
      this.batchAveGrade = getAllAveGrade / this.totalStudents;
    }

    return this;
  }
  //7.
  getBatchMinGrade() {
    if (this.totalStudents) {
      let arrOfMinGrade = [];
      this.sections.forEach(elem => {
        elem.students.forEach(grade => {
          arrOfMinGrade.push(Math.min(...grade.grades));
        });
      });
      this.batchMinGrade = Math.min(...arrOfMinGrade);
    }
    return this;
  }
  //8.
  getBatchMaxGrade() {
    let arrOfMaxGrade = [];
    if (this.totalStudents) {
      this.sections.forEach(elem => {
        elem.students.forEach(grade => {
          arrOfMaxGrade.push(Math.max(...grade.grades));
        });
      });
      this.batchMaxGrade = Math.max(...arrOfMaxGrade);
    }
    return this;
  }
} // ==== end of class Grade ====

const grade1 = new Grade(1);

// add sections to this grade level
grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");

const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Joe", "joe@mail.com", [78, 82, 79, 85]);
section1A.addStudent("Jane", "jane@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

section1B.addStudent("Jeremy", "jeremy@mail.com", [85, 82, 83, 89]);
section1B.addStudent("Johnny", "johnny@mail.com", [82, 86, 77, 88]);
section1B.addStudent("Jerome", "jerome@mail.com", [89, 85, 92, 91]);
section1B.addStudent("Janine", "janine@mail.com", [90, 87, 94, 91]);

section1C.addStudent("Faith", "faith@mail.com", [87, 85, 88, 91]);
section1C.addStudent("Hope", "hope@mail.com", [85, 87, 84, 89]);
section1C.addStudent("Love", "love@mail.com", [91, 87, 90, 88]);
section1C.addStudent("Joy", "joy@mail.com", [92, 86, 90, 89]);

section1D.addStudent("Eddie", "eddie@mail.com", [85, 87, 86, 92]);
section1D.addStudent("Ellen", "ellen@mail.com", [88, 84, 86, 90]);
section1D.addStudent("Edgar", "edgar@mail.com", [90, 89, 92, 86]);
section1D.addStudent("Eileen", "eileen@mail.com", [90, 88, 93, 84]);

/*
1. Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties:
level initialized to passed in number argument
sections initialized to an empty array
totalStudents initialized to zero
totalHonorStudents initialized to zero
batchAveGrade set to undefined
batchMinGrade set to undefined
batchMaxGrade set to undefined 
*/

// 2.Define an addSection() method that will take in a string argument to instantiate a new Section object and push it into the sections array property.

// 3.Write a statement that will add a student to section1A. The student is named John, with email john@mail.com, and grades of 89, 84, 78, and 88.

// 4. Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.

// 5. Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property.

// 6. Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.

// 7. Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.

// 8. Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.
