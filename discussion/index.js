class Student {
  // constructor keyword allows us to pass arguments when we instantiate objects from classes
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;

    this.gradeAve = undefined;

    this.passed = undefined;
    this.passedWithHonors = undefined;

    // ACTIVITY 1 Item 1
    //check first if the array has 4 elements
    if (grades.length === 4) {
      if (grades.every(grade => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }
  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }
  computeAve() {
    let sum = 0;
    this.grades.forEach(grade => (sum = sum + grade));
    // update the gradeAve property
    this.gradeAve = sum / 4;
    // returns the object
    return this;
  }
  // ACTIVITY 1 Item 2
  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }
  // ACTIVITY 1 Item 2
  willPassWithHonors() {
    if (this.passed) {
      if (this.gradeAve >= 90) {
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
}

class Section {
  constructor(name) {
    this.name = name;
    this.students = [];
    this.honorStudents = undefined;
    this.honorsPercentage = undefined;
  }
  addStudent(name, email, grades) {
    this.students.push(new Student(name, email, grades));
    return this;
  }

  countHonorStudents() {
    let count = 0;
    this.students.forEach(student => {
      if (student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
        count++;
      }
    });
    this.honorStudents = count;
    return this;
  }

  computeHonorsPercentage() {
    this.countHonorStudents();
    this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
    return this;
  }
}

const section1A = new Section("section1A");
section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);
section1A.addStudent("John", "john@mail.com", [99, 89, 98, 88]);
section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);
section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);

// if (section1A.students[0]) {
//   console.log(true);
// } else {
//   console.log(false);
// }

/* 
    mini activity
    -create a method that is a replica of com
*/
